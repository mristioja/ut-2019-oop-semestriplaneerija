package ee.ristioja.mai;

public class Õppeaine {
    private String nimi;
    private int eapd;

    public Õppeaine(String nimi, int eapd) {
        this.nimi = nimi;
        this.eapd = eapd;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public int getEapd() {
        return eapd;
    }

    public void setEapd(int eapd) {
        this.eapd = eapd;
    }
}
