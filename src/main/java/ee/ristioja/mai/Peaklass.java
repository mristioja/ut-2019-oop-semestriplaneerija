package ee.ristioja.mai;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Peaklass extends Application  {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage peaLava) {

        Arvutaja arvutaja = new Arvutaja();

        peaLava.setTitle("Semestriplaneerija");

        Button ava = new Button("Ava");
        Button salvesta= new Button("Salvesta");
        salvesta.setOnAction(event -> IO.salvesta(peaLava, arvutaja));
        Button abi = new Button("Abi");
        abi.setOnAction(event -> {
            Alert sõnum = new Alert(Alert.AlertType.INFORMATION);
            sõnum.setTitle("Abi");
            sõnum.setHeaderText("Programmi Semestriplaneerija kasutamine");
            sõnum.setContentText("See programm aitab sul oma semestri ainete mahtu planeerida.");
            sõnum.setResizable(true);

            TextArea kasutamine = new TextArea("Esmalt tuleb sisestada aeg, mis kulub " +
                    "igapäevatoimingutele, seejärel saab sisestada plaanitavad ained koos nende " +
                    "EAP-dega ning vajadusel saab nendest loobuda. Vaikimisi on päevas reserveeritud üks tund " +
                    "ettenägematuteks tegevusteks ja kohustusteks ning on eeldatud, et ainemahu peaks " +
                    "suutma täita 16 nädalaga.");
            kasutamine.setEditable(false);
            kasutamine.setWrapText(true);
            kasutamine.setMaxWidth(Double.MAX_VALUE);
            kasutamine.setMaxHeight(Double.MAX_VALUE);
            sõnum.getDialogPane().setExpandableContent(kasutamine);

            sõnum.showAndWait();
        });

        ToolBar valikuRiba = new ToolBar(
                ava, salvesta, new Separator(),
                abi);

        // igapäevategevuste valimine
        GridPane tegevusteTabel = new GridPane();
        tegevusteTabel.setHgap(10); //vahed tabelilahtrite vahel
        tegevusteTabel.setVgap(5);
        Label unetunnid = new Label("Unetundide arv päevas: ");
        Label hügieen = new Label("Hügieeniks kuluvate tundide arv päevas: ");
        Label transport = new Label("Transpordiks kuluvate tundide arv päevas: ");
        Label söömine = new Label("Söömisele/söögi valmistamisele kuluvate tundide arv päevas: ");
        Label koristamine = new Label("Koristamisele kuluvate tundide arv nädalas: ");
        Label poeskäimine = new Label("Poes käimisele kuluvate tundide arv nädalas: ");
        Label hobid = new Label("Hobidele kuluvate tundide arv nädalas: ");
        Label muu = new Label("Muudele tegevustele kuluvate tundide arv nädalas: ");

        Spinner<Double> uni = new Spinner(0.0, 24.0, 8.0, 0.5);
        Spinner<Double> hüg = new Spinner(0.0, 24.0, 1.0, 0.5);
        Spinner<Double> tr = new Spinner(0.0, 24.0, 1.0, 0.5);
        Spinner<Double> söö = new Spinner(0.0, 24.0, 1.0, 0.5);
        Spinner<Double> kor = new Spinner(0.0, 24.0 * 7.0, 2.0, 0.5);
        Spinner<Double> poe = new Spinner(0.0, 24.0 * 7.0, 3.0, 0.5);
        Spinner<Double> hob = new Spinner(0.0, 24.0 * 7.0, 4.0, 0.5);
        Spinner<Double> muud = new Spinner(0.0, 24.0 * 7.0, 4.5, 0.5);

        arvutaja.undPäevas.bind(uni.valueProperty());
        arvutaja.hügieenPäevas.bind(hüg.valueProperty());
        arvutaja.transportPäevas.bind(tr.valueProperty());
        arvutaja.sööminePäevas.bind(söö.valueProperty());
        arvutaja.koristamineNädalas.bind(kor.valueProperty());
        arvutaja.poesNädalas.bind(poe.valueProperty());
        arvutaja.hobidNädalas.bind(hob.valueProperty());
        arvutaja.muuNädalas.bind(muud.valueProperty());


        ava.setOnAction(event -> {
            IO.avafail(peaLava, arvutaja, uni, hüg, tr, söö, kor, poe, hob, muud);
        });

        tegevusteTabel.add(unetunnid, 0, 0);
        tegevusteTabel.add(hügieen, 0, 1);
        tegevusteTabel.add(transport, 0, 2);
        tegevusteTabel.add(söömine, 0, 3);
        tegevusteTabel.add(koristamine, 0, 4);
        tegevusteTabel.add(poeskäimine, 0, 5);
        tegevusteTabel.add(hobid, 0, 6);
        tegevusteTabel.add(muu, 0, 7);
        tegevusteTabel.add(uni, 1 ,0);
        tegevusteTabel.add(hüg, 1, 1);
        tegevusteTabel.add(tr, 1, 2);
        tegevusteTabel.add(söö, 1, 3);
        tegevusteTabel.add(kor, 1, 4);
        tegevusteTabel.add(poe, 1, 5);
        tegevusteTabel.add(hob, 1, 6);
        tegevusteTabel.add(muud, 1, 7);

        // õppeainete tabel
        TableView tabel = new TableView();
        TableColumn õppeaineVeerg = new TableColumn("Õppeaine");
        õppeaineVeerg.setCellValueFactory(new PropertyValueFactory<>("nimi"));

        TableColumn eapVeerg = new TableColumn("EAP-d");
        eapVeerg.setCellValueFactory(new PropertyValueFactory<>("eapd"));

        tabel.getColumns().addAll(õppeaineVeerg, eapVeerg);
        tabel.setItems(arvutaja.getÕppeained());
        VBox.setVgrow(tabel, Priority.ALWAYS);

        Button lisaAineNupp = new Button("Lisa");
        lisaAineNupp.setOnAction(event -> {
            Õppeaine õppeaine = ÕppeaineSisestamiseAken.näitaAkent("Õppeaine sisestamine", "", 6);
            if (õppeaine != null) {
                arvutaja.lisaÕppeaine(õppeaine);
            }
        });
        Tooltip vihje1 = new Tooltip("Õppeaine lisamine");
        lisaAineNupp.setOnMouseEntered(event -> lisaAineNupp.setTooltip(vihje1));
        Button muudaAineNupp = new Button("Muuda");
        muudaAineNupp.setDisable(true);
        muudaAineNupp.setOnAction(event -> {
            int indeks = tabel.getSelectionModel().getSelectedIndex(); // annab selekteeritud aine indeksi
            Õppeaine vanaÕppeaine = arvutaja.getÕppeained().get(indeks);
            Õppeaine muudetudÕppeaine = ÕppeaineSisestamiseAken.näitaAkent("Õppeaine muutmine", vanaÕppeaine.getNimi(), vanaÕppeaine.getEapd());
            if (muudetudÕppeaine != null)
                arvutaja.getÕppeained().set(indeks, muudetudÕppeaine);
        });

        Button eemaldaAineNupp = new Button("Eemalda");
        eemaldaAineNupp.setDisable(true);
        eemaldaAineNupp.setOnAction(event -> {
            arvutaja.eemaldaÕppeaine(tabel.getSelectionModel().getSelectedIndex());
        });

        // https://stackoverflow.com/a/52977920 :
        // muudab nupud mitteaktiivseks, kui midagi pole selekteeritud
        eemaldaAineNupp.disableProperty().bind(tabel.getSelectionModel().selectedItemProperty().isNull());
        muudaAineNupp.disableProperty().bind(tabel.getSelectionModel().selectedItemProperty().isNull());

        ButtonBar õppeaineNupud = new ButtonBar();
        õppeaineNupud.getButtons().addAll(lisaAineNupp, muudaAineNupp, eemaldaAineNupp);

        VBox õppeaineteKast = new VBox(10, tabel, õppeaineNupud);

        TextArea tulemus = new TextArea();
        tulemus.setEditable(false);
        tulemus.setText(arvutaja.tulemusProperty.get());
        tulemus.textProperty().bind(arvutaja.tulemusProperty);
        Tooltip vihje = new Tooltip("arvutuse tulemus");
        tulemus.setOnMouseMoved(event -> tulemus.setTooltip(vihje));

        Accordion sisestusKastid = new Accordion(
                new TitledPane("Üldaeg", tegevusteTabel),
                new TitledPane("Õppeained", õppeaineteKast),
                new TitledPane("Tulemus", tulemus)
        );
        sisestusKastid.setExpandedPane(sisestusKastid.getPanes().get(0));
        sisestusKastid.setPadding(new Insets(10,10,10,10));

        ScrollPane keskmine = new ScrollPane();
        keskmine.setContent(sisestusKastid);
        keskmine.setFitToHeight(true);
        keskmine.setFitToWidth(true);

        BorderPane aknaSisu = new BorderPane();
        aknaSisu.setTop(valikuRiba);
        aknaSisu.setCenter(keskmine);

        Scene stseen = new Scene(aknaSisu);
        peaLava.setScene(stseen);
        peaLava.show();
    }
}
