package ee.ristioja.mai;

import javafx.application.Platform;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import java.util.Optional;

public class ÕppeaineSisestamiseAken  {

    public static Õppeaine näitaAkent(String tiitel, String nimetus, int eapd) {

        // eeskuju: https://code.makery.ch/blog/javafx-dialogs-official/

        Õppeaine õppeaine;
        Dialog aken = new Dialog();
        aken.setTitle(tiitel);

        Label nimeSilt = new Label("Nimetus:");
        TextField nimeSisend = new TextField(nimetus);
        nimeSisend.setPromptText("Õppeaine nimetus");
        Platform.runLater(() -> nimeSisend.requestFocus());
        Label eapSilt = new Label("EAP-d:");
        Spinner<Integer> eapSisend = new Spinner(1, 24, eapd);

        GridPane tabel = new GridPane();
        tabel.setHgap(10);
        tabel.setVgap(5);
        tabel.add(nimeSilt, 0, 0);
        tabel.add(nimeSisend, 1, 0);
        tabel.add(eapSilt, 0, 1);
        tabel.add(eapSisend, 1, 1);
        aken.getDialogPane().setContent(tabel);

        ButtonType salvestaNupuTüüp = new ButtonType("Salvesta", ButtonBar.ButtonData.OK_DONE);
        ButtonType tühistaNupuTüüp = new ButtonType("Tühista", ButtonBar.ButtonData.CANCEL_CLOSE);
        aken.getDialogPane().getButtonTypes().addAll(salvestaNupuTüüp, tühistaNupuTüüp);

        Node salvestaNupp = aken.getDialogPane().lookupButton(salvestaNupuTüüp);
        salvestaNupp.setDisable(nimetus.isEmpty()); //kui nimetus on tühi, siis ei saa vajutada
        nimeSisend.textProperty().addListener((o, vana, uus) -> {
            salvestaNupp.setDisable(uus.trim().isEmpty());
        });

        aken.setResultConverter(vajutatudNupuTüüp -> { // konstrueerib akna tagastusväärtuse
            if (vajutatudNupuTüüp == salvestaNupuTüüp)
                return new Õppeaine(nimeSisend.getText(), eapSisend.getValue());
            return null;
        });
        Optional<Õppeaine> tulemus = aken.showAndWait(); //saab eelmisest lambdast tulemuse kätte ja tagastab selle
        if (tulemus.isPresent())
            return tulemus.get();
        return null;
    }
}
