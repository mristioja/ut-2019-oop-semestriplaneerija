package ee.ristioja.mai;

import javafx.scene.control.Alert;
import javafx.scene.control.Spinner;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.*;

public class IO {

    public static void avafail(Window peaaken, Arvutaja arvutaja, Spinner<Double> uni, Spinner<Double> hüg,
                               Spinner<Double> tr, Spinner<Double> söö, Spinner<Double> kor, Spinner<Double> poe,
                               Spinner<Double> hob, Spinner<Double> muud) {
        FileChooser failiValija = new FileChooser();
        failiValija.setInitialDirectory(new File(System.getProperty("user.home")));
        failiValija.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Ajaplaneerija 2 failid (*.ap2)", "*.ap2"));
        File fail = failiValija.showOpenDialog(peaaken);
        if (fail == null)
            return;
        try (DataInputStream dis = new DataInputStream(new FileInputStream(fail))) {
            uni.getValueFactory().setValue(dis.readDouble());
            hüg.getValueFactory().setValue(dis.readDouble());
            tr.getValueFactory().setValue(dis.readDouble());
            söö.getValueFactory().setValue(dis.readDouble());
            kor.getValueFactory().setValue(dis.readDouble());
            poe.getValueFactory().setValue(dis.readDouble());
            hob.getValueFactory().setValue(dis.readDouble());
            muud.getValueFactory().setValue(dis.readDouble());
            arvutaja.getÕppeained().clear();
            int listiSuurus = dis.readInt();
            for (int i = 0; i < listiSuurus; i++) {
                String nimetus = dis.readUTF();
                int eapd = dis.readInt();
                arvutaja.lisaÕppeaine(new Õppeaine(nimetus, eapd));
            }
        } catch (Throwable e) {
            Alert veaAken = new Alert(Alert.AlertType.ERROR);
            veaAken.setContentText("Viga faili avamisel: " + e.getMessage());
            veaAken.showAndWait();
        }
    }

    public static void salvesta(Window peaaken, Arvutaja arvutaja) {
        FileChooser failiValija = new FileChooser();
        failiValija.setInitialDirectory(new File(System.getProperty("user.home")));
        failiValija.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Ajaplaneerija 2 failid (*.ap2)", "*.ap2"));
        File fail = failiValija.showSaveDialog(peaaken);
        if (fail == null)
            return;
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(fail))) {
            dos.writeDouble(arvutaja.undPäevas.get());
            dos.writeDouble(arvutaja.hügieenPäevas.get());
            dos.writeDouble(arvutaja.transportPäevas.get());
            dos.writeDouble(arvutaja.sööminePäevas.get());
            dos.writeDouble(arvutaja.koristamineNädalas.get());
            dos.writeDouble(arvutaja.poesNädalas.get());
            dos.writeDouble(arvutaja.hobidNädalas.get());
            dos.writeDouble(arvutaja.muuNädalas.get());
            dos.writeInt(arvutaja.getÕppeained().size());
            for (int i = 0; i < arvutaja.getÕppeained().size(); i++) {
                dos.writeUTF(arvutaja.getÕppeained().get(i).getNimi());
                dos.writeInt(arvutaja.getÕppeained().get(i).getEapd());
            }
        } catch (Throwable e) {
            Alert veaAken = new Alert(Alert.AlertType.ERROR);
            veaAken.setContentText("Viga faili salvestamisel: " + e.getMessage());
            veaAken.showAndWait();
        }
    }

}
