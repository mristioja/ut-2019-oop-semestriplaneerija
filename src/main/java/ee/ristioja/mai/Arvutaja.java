package ee.ristioja.mai;

import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class Arvutaja {

    private static double tundeEaps = 26.0;
    private static double nädalaidSemestris = 16.0;
    private static double tundeNädalas = 24 * 7;
    private static double alatiReserveeritudTundeNädalas = 7;
    public DoubleProperty undPäevas = new SimpleDoubleProperty(8.0);
    public DoubleProperty hügieenPäevas = new SimpleDoubleProperty(1.0);
    public DoubleProperty transportPäevas = new SimpleDoubleProperty(1.0);
    public DoubleProperty sööminePäevas = new SimpleDoubleProperty(1.0);
    public DoubleProperty koristamineNädalas = new SimpleDoubleProperty(2.0);
    public DoubleProperty poesNädalas = new SimpleDoubleProperty(3.0);
    public DoubleProperty hobidNädalas = new SimpleDoubleProperty(4.0);
    public DoubleProperty muuNädalas = new SimpleDoubleProperty(4.5);
    private ObservableList<Õppeaine> õppeained = FXCollections.observableArrayList();
    public StringProperty tulemusProperty = new SimpleStringProperty();

    public Arvutaja() {
        undPäevas.addListener((o, vana, uus) -> arvuta());
        hügieenPäevas.addListener((o, vana, uus) -> arvuta());
        transportPäevas.addListener((o, vana, uus) -> arvuta());
        sööminePäevas.addListener((o, vana, uus) -> arvuta());
        koristamineNädalas.addListener((o, vana, uus) -> arvuta());
        poesNädalas.addListener((o, vana, uus) -> arvuta());
        hobidNädalas.addListener((o, vana, uus) -> arvuta());
        muuNädalas.addListener((o, vana, uus) -> arvuta());

        // https://stackoverflow.com/a/23335675
        õppeained.addListener((ListChangeListener)(c -> arvuta()));

        arvuta();
    }

    private void arvuta() {
        double toiminguTundeNädalas =
                undPäevas.getValue() * 7
                + hügieenPäevas.getValue() * 7
                + transportPäevas.getValue() * 7
                + sööminePäevas.getValue() * 7
                + koristamineNädalas.getValue()
                + poesNädalas.getValue()
                + hobidNädalas.getValue()
                + muuNädalas.getValue();

        //õppimise tunde nädalas:
        double õppimiseTundeNädalas = 0;
        for (Õppeaine e : õppeained) {
            õppimiseTundeNädalas += e.getEapd() * tundeEaps / nädalaidSemestris;
        }

        //palju vaja loobuda:
        double tundeKokku = õppimiseTundeNädalas + toiminguTundeNädalas + alatiReserveeritudTundeNädalas;
        double mitmestEapstVajaLoobuda = 0;
        if (tundeKokku > tundeNädalas) {
            mitmestEapstVajaLoobuda = (int) Math.ceil((tundeKokku - tundeNädalas) * nädalaidSemestris / tundeEaps);
        }

        double vabaAegPäevas = (double)Math.round((tundeNädalas - toiminguTundeNädalas - õppimiseTundeNädalas)/7 * 100)/100;

        tulemusProperty.set("Tunde kokku: " + tundeKokku
                + "\nVaja loobuda: " + mitmestEapstVajaLoobuda + " EAP-st"
                + "\nVaba aeg päevas: " + vabaAegPäevas + " tundi"
                + "\nIgapäevatoimingute tunde nädalas: " + toiminguTundeNädalas);
    }

    public ObservableList<Õppeaine> getÕppeained() {
        return õppeained;
    }

    public DoubleProperty transportPäevasProperty() {
        return transportPäevas;
    }

    public void lisaÕppeaine(Õppeaine õppeaine) {
        õppeained.add(õppeaine);
    }

    public void eemaldaÕppeaine(int indeks) {
        õppeained.remove(indeks);
    }
}